#include "lights.h"

void hsv_to_rgb_lights(uint8_t H, uint8_t S, uint8_t V, uint8_t* red,uint8_t* green,uint8_t* blue) {
	float r = 0, g = 0, b = 0;
	
	float h = (float)H / 255.0f;
	float s = (float)S / 255.0f;
	float v = (float)V / 255.0f;
	
	int i = floor(h * 6);
	float f = h * 6 - i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);
	
	switch (i % 6) {
		case 0: r = v, g = t, b = p; break;
		case 1: r = q, g = v, b = p; break;
		case 2: r = p, g = v, b = t; break;
		case 3: r = p, g = q, b = v; break;
		case 4: r = t, g = p, b = v; break;
		case 5: r = v, g = p, b = q; break;
	}

  *red = (uint8_t)(r*255);
  *green = (uint8_t)(g*255);
  *blue = (uint8_t)(b*255);
}

static void* mem_base;

void init_mem_base() {
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS+SPILED_REG_LED_RGB1_o,8,0);
}

light_struct* init_light(int led_index) {
   light_struct* light = calloc(1,sizeof(light_struct));

    light->id = led_index;
    light->hue = 0;
    light->saturation = 0;
    light->value = 0;

    light->from_hue = 0;
    light->from_saturation = 0;
    light->from_value = 0;

    light->to_hue = 0;
    light->to_saturation = 0;
    light->to_value = 0;

    light->speed = 0;

    light->on = 100;
    light->off = 100;
    light->shift = 0;

    light->is_antiphase = false;
    light->is_blinking = false; 

   return light;
}

void start_light_thread(light_struct* left,light_struct* right) {
    lights_pair_struct* lights_pair = calloc(1,sizeof(lights_pair_struct));
    lights_pair->left = left;
    lights_pair->right = right;

    pthread_t light_thread_id;
    pthread_create(&light_thread_id,NULL,handle_light_thread,lights_pair);

}

uint32_t get_light_color(uint8_t r,uint8_t g,uint8_t b) {
    uint32_t value = 0;
    value += r;
    value = value << 8;
    value += g;
    value = value << 8;
    value += b;
    return value;
}


void static_lights_set(light_struct *light,uint8_t hue,uint8_t saturation,uint8_t value){
    light->hue = hue;
    light->saturation = saturation;
    light->value = value;
    uint8_t r,g,b;
    hsv_to_rgb_lights(hue,value,saturation,&r,&g,&b);

    light->state = STATIC;
}

void dynamic_lights_set(light_struct *light,dynamic_lighting_gui_struct* dynamic_gui) {
    light->from_hue = dynamic_gui->from_cp->hue;
    light->from_saturation = dynamic_gui->from_cp->saturation;
    light->from_value = dynamic_gui->from_cp->value;

    light->to_hue = dynamic_gui->to_cp->hue;
    light->to_saturation = dynamic_gui->to_cp->saturation;
    light->to_value = dynamic_gui->to_cp->value;

    light->speed = dynamic_gui->speed;
    light->state = DYNAMIC;

    light->is_antiphase = false;
}

void blinking_set(light_struct *light,blinking_gui_struct* blinking_gui) {
    light->on = blinking_gui->on;
    light->off = blinking_gui->off;
    light->shift = blinking_gui->shift;
    light->is_blinking = true;
}

bool handle_blinking(light_struct* lights, clock_t time) {
    if((lights->on != 0 || lights->off != 0) && lights->is_blinking) {
        clock_t value = (time + lights->shift) % (lights->on + lights->off);
        if(value > lights->on && lights->is_blinking) {
            return false;
        }
    }
    return true;
}


/*//more fancy than a macro cuz inline
inline uint8_t min(uint8_t a, uint8_t b) {
    if (a > b)
        return b;
    return a;
}*/

void show_static_light(light_struct* lights, clock_t time) {
    uint8_t r,g,b;
    hsv_to_rgb_lights(lights->hue,lights->saturation,lights->value,&r,&g,&b);

    if(handle_blinking(lights, time)) {
        show_light(lights,r,g,b);
    } else {
        show_light(lights,0,0,0);
    }
}

void show_dynamic_light(light_struct* lights, clock_t time) {
    uint8_t f_r,f_g,f_b, t_r,t_g,t_b,r,g,b;
    hsv_to_rgb_lights(lights->from_hue,lights->from_saturation,lights->from_value,&f_r,&f_g,&f_b);
    hsv_to_rgb_lights(lights->to_hue,lights->to_saturation,lights->to_value,&t_r,&t_g,&t_b);
    uint16_t speed = lights->speed;
    float red_bit,green_bit,blue_bit;


    // Unusable but i have emotional connection to this
    // red_bit = min((uint8_t)(f_r - t_r),(uint8_t)(t_r - f_r));
    // green_bit = min((uint8_t)(f_g - t_g),(uint8_t)(t_g - f_g));
    // blue_bit = min((uint8_t)(f_b - t_b),(uint8_t)(t_b - f_b));


    //the change needed for update in millisecond 
    red_bit = (t_r - f_r) / (float)speed;
    green_bit = (t_g - f_g) / (float)speed;
    blue_bit = (t_b - f_b) / (float)speed;


    clock_t mod_time;
    //rising
    if(!lights->is_antiphase) {
        mod_time = time % (speed*2);
    } else {
        mod_time = (2*speed) - (time%(2*speed));
    }

    
    //bunch of weird ifs just for the sake of functionality
    if(!lights->is_antiphase) { //falling
        if(mod_time < speed) {
            // printf("rising\n");
            r = f_r + red_bit*mod_time;
            g = f_g + green_bit*mod_time;
            b = f_b + blue_bit*mod_time;
        } else {
            // printf("falling\n");        
            mod_time -= speed;
            r = t_r - red_bit*mod_time;
            g = t_g - green_bit*mod_time;
            b = t_b - blue_bit*mod_time;
        }
    } else {
        if(mod_time < speed) {
            // printf("rising\n");
            r = t_r - red_bit*mod_time;
            g = t_g - green_bit*mod_time;
            b = t_b - blue_bit*mod_time;
        } else {
            // printf("falling\n");        
            mod_time -= speed;
            r = f_r + red_bit*mod_time;
            g = f_g + green_bit*mod_time;
            b = f_b + blue_bit*mod_time;
        }
    }

    if(handle_blinking(lights, time)) {
        show_light(lights,r,g,b);
    } else {
        show_light(lights,0,0,0);
    }
}



void show_light(light_struct* light,uint8_t r,uint8_t g,uint8_t b) {
    uint32_t value = 0;
    
    // uint8_t red = light->value;
    // uint8_t green = light->saturation;
    // uint8_t blue = light->hue;
    // TODO insert translate to hsv here
    value = get_light_color(r,g,b);
    // value += blue;
    // value = value << 8;
    // value += green;
    // value = value << 8;
    // value += red;
    *(volatile uint32_t*)(mem_base+(light->id*4)) = value;
}

void* handle_light_thread(void *vargp) {
    lights_pair_struct* lights_pair = vargp;

    clock_t counter = clock() / (CLOCKS_PER_SEC / 1000);
    while(1) {
        usleep(100);
        counter = clock() / (CLOCKS_PER_SEC / 1000);
        handle_light_state(lights_pair->left,counter);
        handle_light_state(lights_pair->right,counter);
    }
}

void handle_light_state(light_struct* lights,clock_t time) {
    switch(lights->state) {
        case STATIC:
            show_static_light(lights,time);
            break;
        case DYNAMIC:
            show_dynamic_light(lights,time); 
            break;
        case DYNAMIC_ANTIPHASE:
            break;
    }
}