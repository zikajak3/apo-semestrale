#include "dynamic_lighting_gui.h"

dynamic_lighting_gui_struct* init_dynamic_lighting_gui() {
    dynamic_lighting_gui_struct* dynamic_gui = calloc(1,sizeof(dynamic_lighting_gui_struct));
    dynamic_gui->from_cp = init_color_picker();
    dynamic_gui->to_cp = init_color_picker();        
    dynamic_gui->speed = 150;
    dynamic_gui->state = FROM;
    return dynamic_gui;
}

void change_dynamic_color_picker_setting(dynamic_lighting_gui_struct* dynamic_gui,bool red_clicked) {
    if(red_clicked) {
        switch(dynamic_gui->state) {
            case FROM:
                dynamic_gui->state = TO;
                break;
            case TO:
                dynamic_gui->state = SPEED;
                break;
            case SPEED:
                dynamic_gui->state = FROM;
                break;
        }
    }
}

void update_dynamic_color_picker_value(dynamic_lighting_gui_struct* dynamic_gui, knobs_struct* knobs) {
    switch(dynamic_gui->state) {
    case FROM:
        update_color_picker_value(dynamic_gui->from_cp,knobs);
        break;
    case TO:
        update_color_picker_value(dynamic_gui->to_cp,knobs);
        break;
    case SPEED:
        dynamic_gui->speed += knobs->red_turned*10;
        if(dynamic_gui->speed < 1) {
            dynamic_gui->speed = 1;
        }
        break;
    }
}