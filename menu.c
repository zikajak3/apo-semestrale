
#include "menu.h"

void add_menu_item(menu_struct* menu, char* text, options option) {
    
    menu_item_struct* check_item = menu->first;
    for(int i = 0; i < menu->count; i++) {
        if(strcmp(text,check_item->text) == 0) {
            check_item->text = text;
            return NULL;
        }
        check_item = check_item->next;
    }

    menu_item_struct* item = calloc(1,sizeof(menu_item_struct));
    if(menu->first == 0 && menu->last == 0) {
        printf("setting active item %s\n",text);
        menu->first = item;
        menu->last = item;
        item->active = true;
        menu->active = item;
    } else {
        item->active = false;
    }
    item->next = menu->first;
    item->previous = menu->last;

    menu->last->next = item;
    menu->last = item;
    menu->first->previous = item;

    item->text = text;
    item->option = option;

    menu->count++;
}

menu_struct* init_menu() {
    menu_struct* menu = calloc(1,sizeof(menu_struct));
    menu->first = 0;
    menu->last = 0;
    menu->count = 0;
    return menu;    
}

menu_struct* create_menu() {
    menu_struct* menu = init_menu();

    add_menu_item(menu,"set left",SET_LEFT);
    add_menu_item(menu,"set right",SET_RIGHT);
    add_menu_item(menu,"set both",SET_BOTH);
    add_menu_item(menu,"copy left",COPY_LEFT);
    add_menu_item(menu,"copy right",COPY_RIGHT);
    return menu;
}

void handle_options_turned(menu_struct* menu,int8_t turned) {
    if(turned != 0) {
        menu_item_struct* item = menu->active;
        item->active= false;
        if(turned > 0) {
            for(int i = 0; i < turned;i++) {
                item = item->next;
            }
        } else {
            for(int i = 0; i > turned;i--) {
                item = item->previous;
            }
        }
        menu->active = item;
        item->active = true;
    }
}
