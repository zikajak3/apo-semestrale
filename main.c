#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "menu.h"
#include "color_picker.h"
#include "potenciometer.h"
#include "lights.h"
#include "lcd.h"
#include "view.h"
#include "colors.h"
#include "blinking_gui.h"

#include "master.h"
#include "slave.h"

// all possible states of the application
typedef enum {
  MAIN_MENU,
  STATIC_COLOR_MENU,
  DYNAMIC_COLOR_MENU,
  BLINKING_COLOR_MENU,
  MASTER_MENU,
  COLOR_PICKER_LEFT,
  COLOR_PICKER_RIGHT,
  COLOR_PICKER_BOTH,
  DYNAMIC_COLOR_PICKER_LEFT,
  DYNAMIC_COLOR_PICKER_RIGHT,
  DYNAMIC_COLOR_PICKER_BOTH,
  DYNAMIC_COLOR_PICKER_BOTH_ANTIPHASE,
  BLINKING_COLOR_LEFT,
  BLINKING_COLOR_RIGHT,  
  BLINKING_COLOR_BOTH,
  ENSLAVED,
} states;

static states state;

static states previous_state;

static bool large_mode;

//structs for menu and submenus
menu_struct* main_menu;
menu_struct* static_menu;
menu_struct* dynamic_menu;
menu_struct* blinking_menu;
menu_struct* master_menu;

color_picker_struct* color_picker;

//structs used when restoring the last effect
light_struct* backup_left_light;
light_struct* backup_right_light;

//structs for LED diodes
light_struct* left_light;
light_struct* right_light;

dynamic_lighting_gui_struct* dynamic_picker;
blinking_gui_struct* blinking_gui;

knobs_struct *knobs;

//ethernet communication bools
bool is_slave;
bool is_master;
bool is_connected;
 
void set_state(states newState) {
  previous_state = state;
  state = newState;
}

void menu_pressed(menu_struct* menu) {
  if(knobs->blue_clicked) {
      options option = menu->active->option;
      if( //creating backup of the current LEFT light
        option == SET_LEFT ||
        option == SET_BOTH ||
        option == SET_LEFT_DYNAMIC ||
        option == SET_BOTH_DYNAMIC ||
        option == SET_BOTH_DYNAMIC_ANTIPHASE ||
        option == SET_LEFT_BLINKING ||
        option == SET_BOTH_BLINKING 
      ) {
        memcpy(backup_left_light,left_light, sizeof(light_struct));
      }
      if( //creating backup of the current RIGHT light
        option == SET_RIGHT ||
        option == SET_BOTH ||
        option == SET_RIGHT_DYNAMIC ||
        option == SET_BOTH_DYNAMIC ||
        option == SET_BOTH_DYNAMIC_ANTIPHASE ||
        option == SET_RIGHT_BLINKING  ||
        option == SET_BOTH_BLINKING         
      ) {
        memcpy(backup_right_light,right_light, sizeof(light_struct));
      }

      uint8_t num;
      //handling all the menu clicked options
      switch(menu->active->option) { 

        case SET_STATIC:
          set_state(STATIC_COLOR_MENU);
          break;

        case SET_DYNAMIC:
          set_state(DYNAMIC_COLOR_MENU);
          break;

        case SET_BLINKING:
          set_state(BLINKING_COLOR_MENU);
          break;

        case BE_MASTER:
          is_master = true;
          set_state(MASTER_MENU);
          start_master(&is_slave);
          break;

        case SET_LEFT:
            set_state(COLOR_PICKER_LEFT);
            color_picker->hue = left_light->hue;
            color_picker->saturation = left_light->saturation;
            color_picker->value = left_light->value;
          break;

        case SET_RIGHT:
            set_state(COLOR_PICKER_RIGHT);
            color_picker->hue = right_light->hue;
            color_picker->saturation = right_light->saturation;
            color_picker->value = right_light->value;
          break;

        case SET_BOTH:
            set_state(COLOR_PICKER_BOTH);
          break;
        case SET_LEFT_DYNAMIC:
            set_state(DYNAMIC_COLOR_PICKER_LEFT);
          break;
        case SET_RIGHT_DYNAMIC:
            set_state(DYNAMIC_COLOR_PICKER_RIGHT);        
          break;
        case SET_BOTH_DYNAMIC:
            set_state(DYNAMIC_COLOR_PICKER_BOTH);        
          break;
        case SET_BOTH_DYNAMIC_ANTIPHASE:
            set_state(DYNAMIC_COLOR_PICKER_BOTH_ANTIPHASE);
          break;
        case SET_LEFT_BLINKING:
          set_state(BLINKING_COLOR_LEFT);
          blinking_gui->on = left_light->on;
          blinking_gui->off = left_light->off;
          blinking_gui->shift = left_light->shift;
          left_light->is_blinking = true;
          break;
        case SET_RIGHT_BLINKING:
          set_state(BLINKING_COLOR_RIGHT);
          blinking_gui->on = right_light->on;
          blinking_gui->off = right_light->off;
          blinking_gui->shift = right_light->shift;
          right_light->is_blinking = true;
          break;
        case SET_BOTH_BLINKING:
          set_state(BLINKING_COLOR_BOTH);
          left_light->is_blinking = true;
          right_light->is_blinking = true;
          break;
        case COPY_LEFT:
          memcpy(right_light,left_light,sizeof(light_struct));
          right_light->id = 1;
          break;

        case COPY_RIGHT:
          memcpy(left_light,right_light,sizeof(light_struct));
          left_light->id = 0;
          break;

        case DISABLE_BLINKING_LEFT:
          left_light->is_blinking = false;
          break;

        case DISABLE_BLINKING_RIGHT:
          right_light->is_blinking = false;
          break;

        case BACK:
          set_state(previous_state);
          break;

        case BACK_MAIN_MENU:
          set_state(MAIN_MENU);
          break;

        case ENSLAVE:
          memcpy(&num,menu->active->text+25,1);
          printf("%d\n",num);
          is_master = true;
          start_enslave((uint8_t *)menu->active->text+25,left_light,right_light);
          break;

        case RELEASE:
          is_master = false;
          break;
        case RESTORE_LEFT:
          memcpy(left_light,backup_left_light, sizeof(light_struct));
          break;
        case RESTORE_RIGHT:
          memcpy(right_light,backup_right_light, sizeof(light_struct));
          break;
        case RESTORE_BOTH:
          memcpy(left_light,backup_left_light, sizeof(light_struct));
          memcpy(right_light,backup_right_light, sizeof(light_struct));
          break;
        default:
          printf("Unknown option\n");
          break;
      }
    } 
}

void create_main_menu(menu_struct* menu) {
    add_menu_item(menu,"set light",SET_STATIC);
    add_menu_item(menu,"copy/restore",SET_DYNAMIC);
    add_menu_item(menu,"set blinking",SET_BLINKING);
    add_menu_item(menu,"connection",BE_MASTER);
}

void create_static_light_menu(menu_struct* menu) {
    add_menu_item(menu,"static-left",SET_LEFT);
    add_menu_item(menu,"static-right",SET_RIGHT);
    add_menu_item(menu,"static-both",SET_BOTH);

    add_menu_item(menu,"dynamic-left",SET_LEFT_DYNAMIC);
    add_menu_item(menu,"dynamic-right",SET_RIGHT_DYNAMIC);
    add_menu_item(menu,"dynamic-both",SET_BOTH_DYNAMIC);
    add_menu_item(menu,"dynamic-both antiphase",SET_BOTH_DYNAMIC_ANTIPHASE);
    
    add_menu_item(menu,"back",BACK_MAIN_MENU);
}

void create_dynamic_light_menu(menu_struct* menu) {
    add_menu_item(menu,"copy-left",COPY_LEFT);
    add_menu_item(menu,"copy-right",COPY_RIGHT);
    add_menu_item(menu,"restore-left",RESTORE_LEFT);
    add_menu_item(menu,"restore-right",RESTORE_RIGHT);
    add_menu_item(menu,"restore-both",RESTORE_BOTH);
    add_menu_item(menu,"back",BACK_MAIN_MENU);
}

void create_blinking_light_menu(menu_struct* menu) {
    add_menu_item(menu,"set-left",SET_LEFT_BLINKING);
    add_menu_item(menu,"set-right",SET_RIGHT_BLINKING);    
    add_menu_item(menu,"set-both",SET_BOTH_BLINKING);
    add_menu_item(menu,"disable blinking-left",DISABLE_BLINKING_LEFT);
    add_menu_item(menu,"disable blinking-right",DISABLE_BLINKING_RIGHT);
    add_menu_item(menu,"back",BACK_MAIN_MENU);
}

void create_master_menu(menu_struct* menu) {
    add_menu_item(menu,"back",BACK_MAIN_MENU);
    add_menu_item(menu,"release slave",RELEASE);
}

void render_state(states rendered_state);

void render_state(states rs) {

  if(rs == COLOR_PICKER_LEFT ||
    rs == COLOR_PICKER_RIGHT ||
    rs == COLOR_PICKER_BOTH ||
    rs == DYNAMIC_COLOR_PICKER_LEFT ||
    rs == DYNAMIC_COLOR_PICKER_RIGHT ||
    rs == DYNAMIC_COLOR_PICKER_BOTH ||
    rs == DYNAMIC_COLOR_PICKER_BOTH_ANTIPHASE ||
    rs == BLINKING_COLOR_LEFT ||
    rs == BLINKING_COLOR_RIGHT ||
    rs == BLINKING_COLOR_BOTH
    ) {
      render_state(previous_state);
      dim_lcd(100);
    }

  switch(rs) {
      case MAIN_MENU:
        add_text_lcd(10,10,"MAIN MENU",true,WHITE);
        render_menu(main_menu,20,40,large_mode);
        break;

      case STATIC_COLOR_MENU:
        add_text_lcd(10,10,"SET LIGHTING",true,WHITE);
        render_menu(static_menu,20,40,large_mode);
        break;

      case DYNAMIC_COLOR_MENU:
        add_text_lcd(10,10,"COPY/RESTORE",true,WHITE);      
        render_menu(dynamic_menu,20,40,large_mode);
        break;

      case BLINKING_COLOR_MENU:
        add_text_lcd(10,10,"BLINKING",true,WHITE);      
        render_menu(blinking_menu,20,40,large_mode);       
        break;

      case MASTER_MENU:
        add_text_lcd(10,10,"MASTER MENU",true,WHITE);      
        render_menu(master_menu,20,40,large_mode);       
        break;

      case COLOR_PICKER_LEFT:
        render_color_picker(color_picker,15,35,large_mode);
        add_text_lcd(280,30,"LEFT",true,WHITE);
        break;

      case COLOR_PICKER_RIGHT:
        render_color_picker(color_picker,15,35,large_mode);
        add_text_lcd(280,30,"RIGHT",true,WHITE);
        break;

      case COLOR_PICKER_BOTH:        
        render_color_picker(color_picker,15,35,large_mode);
        add_text_lcd(280,30,"BOTH",true,WHITE);
        break;
      
      case DYNAMIC_COLOR_PICKER_LEFT:
        // render_dynamic_ligting_gui(dynamic_picker,15,35,large_mode);
        // break;
      case DYNAMIC_COLOR_PICKER_RIGHT:
        // render_dynamic_ligting_gui(dynamic_picker,15,35,large_mode);
        // break;
      case DYNAMIC_COLOR_PICKER_BOTH:
        // render_dynamic_ligting_gui(dynamic_picker,15,35,large_mode);
        // break;
      case DYNAMIC_COLOR_PICKER_BOTH_ANTIPHASE:
        render_dynamic_ligting_gui(dynamic_picker,15,35,large_mode);
        break;
      case BLINKING_COLOR_LEFT:
      case BLINKING_COLOR_RIGHT:
      case BLINKING_COLOR_BOTH:
        render_blinking_gui(blinking_gui,15,35,large_mode);
        break;
      case ENSLAVED:
        add_text_lcd(80,80,"ENSLAVED",true,WHITE);
        add_text_lcd(80,120,"press blue to release",true,WHITE);
        break;
      default:
        break;

    }
}

void init_menus() {
  main_menu = init_menu();
  static_menu = init_menu();
  dynamic_menu = init_menu();
  blinking_menu = init_menu();
  master_menu = init_menu();

  create_main_menu(main_menu);
  create_static_light_menu(static_menu);
  create_dynamic_light_menu(dynamic_menu);
  create_blinking_light_menu(blinking_menu);
  create_master_menu(master_menu);
}

void init_lights_pickers() {
  color_picker = init_color_picker();
  left_light = init_light(0);
  right_light = init_light(1);
  
  backup_left_light = init_light(0);
  backup_right_light = init_light(1);
  
  dynamic_picker = init_dynamic_lighting_gui();

  show_static_light(left_light,0);
  show_static_light(right_light,0);
}

void init() {
  init_menus();
  init_mem_base();
  init_lights_pickers();
  knobs = init_knobs();
  large_mode = false;
  blinking_gui = init_blinking();
  init_lcd();
  init_pressed(&(knobs->blue_pressed));
  is_master = false;
  is_slave = false;
  is_connected = false;

  init_master(&is_slave,&is_master, &is_connected,master_menu);
  init_slave(&is_master,&is_slave,&is_connected,left_light,right_light);

  start_slave();
}

int main(int argc, char *argv[])
{ 
  //uint16_t white = get_color(255,255,255);

  init();
  state = MAIN_MENU;

  clock_t counter = clock() / (CLOCKS_PER_SEC / 1000);
  clock_t start  = counter;
  clock_t end  = counter;
  long int frames = 0;

  start_light_thread(left_light,right_light);

  while(1) {
    

    update_knob_values(knobs);
    
    if(knobs->green_clicked) {
      large_mode = !large_mode;
      switch_large_mode();
    }

    if(state == COLOR_PICKER_LEFT ||
      state ==COLOR_PICKER_RIGHT ||
      state ==COLOR_PICKER_BOTH ||
      state ==DYNAMIC_COLOR_PICKER_LEFT ||
      state ==DYNAMIC_COLOR_PICKER_RIGHT ||
      state ==DYNAMIC_COLOR_PICKER_BOTH ||
      state ==DYNAMIC_COLOR_PICKER_BOTH_ANTIPHASE ||
      state ==BLINKING_COLOR_LEFT ||
      state ==BLINKING_COLOR_RIGHT ||
      state ==BLINKING_COLOR_BOTH
      ) {
        if(knobs->blue_clicked) {
          set_state(previous_state);
          continue;
        }
      }

    if(is_slave) {
      if(state != ENSLAVED) {
        set_state(ENSLAVED);
      }
    } else if(state == ENSLAVED){
      set_state(previous_state);
    }

    switch(state) {
      case MAIN_MENU:
        handle_options_turned(main_menu,knobs->blue_turned);
        menu_pressed(main_menu);
        break;

      case STATIC_COLOR_MENU:
        handle_options_turned(static_menu,knobs->blue_turned);
        menu_pressed(static_menu);
        break;

      case DYNAMIC_COLOR_MENU:
        handle_options_turned(dynamic_menu,knobs->blue_turned);
        menu_pressed(dynamic_menu);
        break;

      case BLINKING_COLOR_MENU:
        handle_options_turned(blinking_menu,knobs->blue_turned);
        menu_pressed(blinking_menu);
        break;

        case MASTER_MENU:
        handle_options_turned(master_menu,knobs->blue_turned);
        menu_pressed(master_menu);
        break;

      case COLOR_PICKER_LEFT:
        update_color_picker_value(color_picker,knobs);
        static_lights_set(left_light,color_picker->hue,color_picker->saturation,color_picker->value);
        break;

      case COLOR_PICKER_RIGHT:
        update_color_picker_value(color_picker,knobs);
        static_lights_set(right_light,color_picker->hue,color_picker->saturation,color_picker->value);
        break;

      case COLOR_PICKER_BOTH:
        update_color_picker_value(color_picker,knobs);
        static_lights_set(left_light,color_picker->hue,color_picker->saturation,color_picker->value);
        static_lights_set(right_light,color_picker->hue,color_picker->saturation,color_picker->value);        
        break;

      case DYNAMIC_COLOR_PICKER_LEFT:
        update_dynamic_color_picker_value(dynamic_picker,knobs);
        change_dynamic_color_picker_setting(dynamic_picker,knobs->red_clicked);
        dynamic_lights_set(left_light,dynamic_picker);
        break;

      case DYNAMIC_COLOR_PICKER_RIGHT:
        update_dynamic_color_picker_value(dynamic_picker,knobs);
        change_dynamic_color_picker_setting(dynamic_picker,knobs->red_clicked);
        dynamic_lights_set(right_light,dynamic_picker);        
        break;

      case DYNAMIC_COLOR_PICKER_BOTH:
        update_dynamic_color_picker_value(dynamic_picker,knobs);
        change_dynamic_color_picker_setting(dynamic_picker,knobs->red_clicked);
        dynamic_lights_set(left_light,dynamic_picker);        
        dynamic_lights_set(right_light,dynamic_picker); 
        break;

      case DYNAMIC_COLOR_PICKER_BOTH_ANTIPHASE:
        update_dynamic_color_picker_value(dynamic_picker,knobs);
        change_dynamic_color_picker_setting(dynamic_picker,knobs->red_clicked);
        dynamic_lights_set(left_light,dynamic_picker);        
        dynamic_lights_set(right_light,dynamic_picker);
        right_light->is_antiphase = true;
        break;
      
      case BLINKING_COLOR_LEFT:
        update_blinking_value(blinking_gui,knobs);
        blinking_set(left_light,blinking_gui);
        break;

      case BLINKING_COLOR_RIGHT:
        update_blinking_value(blinking_gui,knobs);
        blinking_set(right_light,blinking_gui);
        break;

      case BLINKING_COLOR_BOTH:
        update_blinking_value(blinking_gui,knobs);
        blinking_set(left_light,blinking_gui);
        blinking_set(right_light,blinking_gui);
        break;
      case ENSLAVED:
        if(knobs->blue_clicked) {
          set_state(previous_state);
          is_slave = false;
        }
        break;
      default:
        state = MAIN_MENU;
        break;
    }

    counter = clock() / (CLOCKS_PER_SEC / 1000);

    if(!is_rendering() && frames % 5000 == 0) {
      start = counter;
      clear_lcd();      
      render_status_bar(left_light,right_light,0,0,is_connected && is_master,large_mode);
      render_state(state);
      char fps[20];
      sprintf(fps,"%ld",end);
      // add_text_lcd(WIDTH-30,10,fps,true,YELLOW);
      display_lcd();
      counter = clock() / (CLOCKS_PER_SEC / 1000);      
      end = counter - start;
    }

    counter = clock() / (CLOCKS_PER_SEC / 1000);
    frames++;
  }
  return 0;
}
