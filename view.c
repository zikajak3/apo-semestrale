#include "view.h"
#include "lcd.h"
// #include "colors.h"

bool* pressed;

void init_pressed(bool* pressed_pnt) {
    pressed = pressed_pnt;
}

void render_square(int x, int y, int size, uint16_t color) {
    render_rectangle(x,y,size,size,color);
}

void render_rectangle(int x, int y, int width,int height, uint16_t color) {
    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            write_pixel_lcd(x+j,y+i,color);
        }
    }
}

void render_triangle_top_left(int x, int y, int size, uint16_t color) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size - i; j++) {
            write_pixel_lcd(x+j+1,y+i,color);
        }
    }
}

void render_triangle_bottom_right(int x, int y, int size, uint16_t color) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < i; j++) {
            write_pixel_lcd(x+(size-j),y+i,color);
        }
    }
}

void render_empty_square(int x, int y, int size, uint16_t color) {
    for(int i = 0; i < size; i++) {
        write_pixel_lcd(x+i,y,color);
        write_pixel_lcd(x,y+i,color);

        write_pixel_lcd(x+i,y+size,color);        
        write_pixel_lcd(x+size,y+i,color);
    }
    
}

void render_menu(menu_struct* menu, int baseX, int baseY,bool large_mode) {
    int x = baseX;
    int y = baseY;
    
    uint16_t chosen_color = get_color(0xe0,0xc7,0x91);
    uint16_t pressed_color = get_color(0xba,0xa0,0x6a);
    uint16_t not_chosen_color = get_color(255,255,255);
    int offset = large_mode ? 34 : 20;

    menu_item_struct* item;
    int items;
    if(!large_mode) {
        item = menu->first;
        items = menu->count;
    } else {
        item = menu->first;
        items = menu->count;
        if(menu->count > 6) {
            items = 6;
            item = menu->active;
        }
    }


    for(int i = 0; i < items; i++) {
        uint16_t color = item->active ? *pressed ? pressed_color: chosen_color : not_chosen_color;
        add_text_lcd(x,y,item->text,false,color);
        if(item->active) {
            if(!large_mode) {
                render_rectangle(x-14,y+9,8,1,WHITE);
            } else {
                render_rectangle(x-16,y+16,10,4,WHITE);
            }
        }
        item = item->next; 
        y += offset;
    }
}

void render_status_bar(light_struct* left_light, light_struct* right_light,int baseX, int baseY,bool is_master,bool large_mode) {
    //uint16_t left_color = get_hsv_color(left_light->hue,left_light->saturation,left_light->value);
    //uint16_t right_color = get_hsv_color(right_light->hue,right_light->saturation,right_light->value);

    int x, y;
    x = 60;
    y = HEIGHT-20-35;

    char text[20];

    int offset = large_mode ? 24 : 12;


    //  LEFT LIGHT
    x = 60;
    y = HEIGHT-20-(2*offset);

    if(is_master) {
        int master_pos = large_mode ? 8*offset : 16*offset;
        add_text_lcd(master_pos,y,"master",false,WHITE);
    }

    //hue
    if(left_light->state == STATIC) {
        add_text_lcd(x,y,"static",false,WHITE);
    } else {
        add_text_lcd(x,y,"dynamic",false,WHITE);
    }
    y += offset;
    if(left_light->is_blinking) {        
        add_text_lcd(x,y,"blink",false,WHITE);
    }
    y += offset;
    
    dim_rect_lcd(20+3,HEIGHT-20-30+3, 30,30,20);

    if(left_light->state == STATIC) {
        render_square(20,HEIGHT-20-30, 30,get_hsv_color(left_light->hue,left_light->saturation,left_light->value));
    }
    else if(left_light->state == DYNAMIC) {
        render_triangle_top_left(20,HEIGHT-20-30, 30,get_hsv_color(left_light->from_hue,left_light->from_saturation,left_light->from_value));        
        render_triangle_bottom_right(20,HEIGHT-20-30, 30,get_hsv_color(left_light->to_hue,left_light->to_saturation,left_light->to_value));        
    }
    
    

    //  RIGHT LIGHT

    x = WIDTH-120;
    if(large_mode) {
        x -= 70;
    }
    y = HEIGHT-20-(2*offset);
    //hue
    
    if(right_light->state == STATIC) {
        add_text_lcd(x,y,"static",false,WHITE);
    } else {
        add_text_lcd(x,y,"dynamic",false,WHITE);
    }
    y += offset;
    if(right_light->is_blinking) {        
        add_text_lcd(x,y,"blink",false,WHITE);
    }
    y += offset;
    // sprintf(text,"%3u:v",right_light->value);
    // add_text_lcd(x,y,text,false,WHITE);
    y += offset;

    if(!large_mode) {
        render_rectangle(10,260,WIDTH-20,1,WHITE);
    }

    dim_rect_lcd(WIDTH-50+3,HEIGHT-20-30+3, 30,30,20);
    if(right_light->state == STATIC) {
        render_square(WIDTH-50,HEIGHT-20-30, 30,get_hsv_color(right_light->hue,right_light->saturation,right_light->value));
    }
    else if(right_light->state == DYNAMIC) {
        render_triangle_top_left(WIDTH-50,HEIGHT-20-30, 30,get_hsv_color(right_light->from_hue,right_light->from_saturation,right_light->from_value));
        render_triangle_bottom_right(WIDTH-50,HEIGHT-20-30, 30,get_hsv_color(right_light->to_hue,right_light->to_saturation,right_light->to_value));
    }
    // render_square(WIDTH-50,HEIGHT-20-30, 30,right_color);
    
}

void render_color_picker(color_picker_struct* color_picker, int baseX,int baseY,bool large_mode) {


    uint16_t color = get_hsv_color(color_picker->hue,color_picker->saturation,color_picker->value);
    //uint16_t white = get_color(255,255,255);
    int x = baseX;
    int y = baseY;

    if(!large_mode) {
        dim_rect_lcd(240+8,15+8,WIDTH/2-30,170,20);
        render_rectangle(240,15,WIDTH/2-30,170,WINDOW_COLOR);
    } else {
        dim_rect_lcd(210+8,15+8,WIDTH/2,170,20);
        render_rectangle(210,15,WIDTH/2,170,WINDOW_COLOR);
    }

    dim_rect_lcd(WIDTH-baseX-120+5,baseY+5+30, 80,80,20);
    dim_rect_lcd(WIDTH-baseX-120+8,baseY+8+30, 74,74,20);    
    render_square(WIDTH-baseX-120,baseY+30, 80,color);
    // render_triangle_bottom_right(WIDTH-baseX-120,baseY+30, 80,WHITE);
    // render_empty_square(WIDTH-baseX-100+5,baseY+5,80,0);
    // render_empty_square(WIDTH-baseX-100,baseY,80,0);
    
    int offset = large_mode ? 30 : 20;

    char text[20];
    //hue
    if(!large_mode) {
        x = WIDTH-200;
    } else {
        x = WIDTH-250;
    }
    y+=40;
    sprintf(text,"h:%u",color_picker->hue);
    add_text_lcd(x,y,text,false,0);
    y += offset;

    //saturation
    sprintf(text,"s:%u",color_picker->saturation);
    add_text_lcd(x,y,text,false,0);
    y += offset;

    //saturation
    sprintf(text,"v:%u",color_picker->value);
    add_text_lcd(x,y,text,false,0);
    y += offset;

    render_exit_button(WIDTH-50,baseY-10);
}

void render_dynamic_ligting_gui(dynamic_lighting_gui_struct* dynamic_gui, int baseX, int baseY, bool large_mode) {
    int x = baseX;
    int y = baseY;
    int offset = large_mode ? 30 : 20;

    int x_offset = large_mode ? 38 : 20;        
    
    dynamic_lighting_states state = dynamic_gui->state;


    color_picker_struct* from_picker = dynamic_gui->from_cp;
    color_picker_struct* to_picker = dynamic_gui->to_cp;
    //color_picker_struct* active_picker = dynamic_gui->state == FROM ? from_picker : dynamic_gui->state == TO ? to_picker : NULL;
    
    uint16_t from_color = get_hsv_color(from_picker->hue,from_picker->saturation,from_picker->value);
    uint16_t to_color = get_hsv_color(to_picker->hue,to_picker->saturation,to_picker->value);


    if(!large_mode) {
        dim_rect_lcd(240+8-2*offset,15+8,(WIDTH/2)+1*offset,220+2*offset,30);
        render_rectangle(240-2*offset,15,(WIDTH/2)+1*offset,220+2*offset,WINDOW_COLOR);
    } else {
        render_rectangle(WIDTH/3,0,WIDTH/3*2,HEIGHT,WINDOW_COLOR);
    }


    // render from color
    dim_rect_lcd(WIDTH-x-120+5,y+5+30, 80,80,20);
    dim_rect_lcd(WIDTH-x-120+8,y+8+30, 74,74,20);
    render_triangle_top_left(WIDTH-x-120,y+30, 80,from_color);
    render_triangle_bottom_right(WIDTH-x-120,y+30, 80,to_color);

    // y += 135;

    

    // FROM PICKER
    // if(active_picker) {
    y = 75;
    char text[20];
    //hue
    x= large_mode ? WIDTH - 280 : WIDTH-200;
    // y+=40;
    sprintf(text,"%u",from_picker->hue);
    
    add_text_lcd(x,y,"h:",false,state == FROM ? RED : BLACK);
    add_text_lcd(x+x_offset,y,text,false,0);
    y += offset;

    //saturation
    sprintf(text,"%u",from_picker->saturation);
    add_text_lcd(x,y,"s:",false,state == FROM ? GREEN : BLACK);
    add_text_lcd(x+x_offset,y,text,false,0);
    y += offset;

    //saturation
    sprintf(text,"%u",from_picker->value);
    add_text_lcd(x,y,"v:",false,state == FROM ? BLUE : BLACK);
    add_text_lcd(x+x_offset,y,text,false,0);

    y += offset*2;

    // TO PICKER
    // y = 75;
    //hue
    x= WIDTH-120;
    // y+=40;
    sprintf(text,"%u",to_picker->hue);
    add_text_lcd(x,y,"h:",false,state == TO ? RED : BLACK);
    add_text_lcd(x+x_offset,y,text,false,0);
    y += offset;

    //saturation
    sprintf(text,"%u",to_picker->saturation);
    add_text_lcd(x,y,"s:",false,state == TO ? GREEN : BLACK);
    add_text_lcd(x+x_offset,y,text,false,0);
    y += offset;

    //saturation
    sprintf(text,"%u",to_picker->value);
    add_text_lcd(x,y,"v:",false,state == TO ? BLUE : BLACK);
    add_text_lcd(x+x_offset,y,text,false,0);
    y += offset;

    // render speed
    char speed[20];
    sprintf(speed,"speed:%d ms",dynamic_gui->speed);
    uint16_t speed_color = 0;
    if(dynamic_gui->state == SPEED) {
        speed_color = get_color(0xff,0x00,0x00);
    }
    add_text_lcd(x-4*offset,y,speed,false,speed_color);

    render_exit_button(WIDTH-50,baseY-10);
    
    // // render to color
    // baseY += 200;
    // render_square(WIDTH-baseX-120+5,baseY+5+30, 80,DARKER_BG);
    // render_square(WIDTH-baseX-120+8,baseY+8+30, 74,DARK_BG);
    // render_triangle_top_left(WIDTH-baseX-120,baseY+30, 80,from_color);
    // render_triangle_bottom_right(WIDTH-baseX-120,baseY+30, 80,WHITE);

}

void render_blinking_gui(blinking_gui_struct* blinking_gui, int baseX, int baseY, bool large_mode) {

    char text[20];

    int x = large_mode ? WIDTH-250 : WIDTH-160;
    int y = 30;
    int offset = large_mode ? 35 : 20;

    int width = large_mode ? 220 : 185; 
    int height = large_mode ? 150 : 100;

    dim_rect_lcd(WIDTH-width+8-30,15+8,width,height,20);
    render_rectangle(WIDTH-width-30,15,width,height,WINDOW_COLOR);


    sprintf(text,"on:%d ms",blinking_gui->on);
    add_text_lcd(x,y,text,false,WHITE);
    y += offset;

    sprintf(text,"of:%d ms",blinking_gui->off);
    add_text_lcd(x,y,text,false,WHITE);
    y += offset;

    sprintf(text,"shift:%d ms",blinking_gui->shift);
    add_text_lcd(x,y,text,false,WHITE);
    y += offset;

    render_exit_button(WIDTH-50,baseY-10);
}

// void render_color_picker() {

// }

void render_exit_button(int x, int y) {
    uint16_t color = *pressed ? get_color(0x1c,0x38,0x87) : get_color(0x32,0x61,0xe5);
    add_text_lcd(x,y,"x",true,color);
}