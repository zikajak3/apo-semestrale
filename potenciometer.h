#ifndef POTENCIOMETER
#define POTENCIOMETER
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "mzapo_phys.h"
#include "mzapo_regs.h"

/**
 * knobs_struct
 * 
 * contains params describing how much user turned each one of the knobs
 * *color*_clicked : describes whether user clicked the button,
 * *color*_pressed : describes whether user is pressing the button or not
 * previous_*color*: the value knob had before
 * 
 * @param mem_base : the assigned memory
 *
 */  
typedef struct {
    int8_t red_turned;
    int8_t green_turned; 
    int8_t blue_turned;
    bool red_pressed; 
    bool green_pressed;
    bool blue_pressed;
    bool red_clicked,green_clicked,blue_clicked;
    uint8_t previous_red,previous_green,previous_blue; //private - just dont

    void* mem_base;
} knobs_struct;

/**
 * function init_knobs
 * 
 * sets memory for the knobs structure, 
 * (*color*_turned = 2, *color*_pressed = false)
 */ 
knobs_struct* init_knobs();

//mozna odstranit
void update_values(knobs_struct* knobs);


/**
 * function update_knob_values
 * 
 * updates @param knobs, click/press/turned etc.
 */ 
void update_knob_values(knobs_struct* knobs);
#endif
