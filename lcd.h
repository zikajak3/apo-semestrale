#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "mzapo_parlcd.h"
#include "pthread.h"

#include "font_types.h"

#define WIDTH 480
#define HEIGHT 320

typedef uint16_t lcd_pixel; //defined variable for one pixel

/**
 * lcd_struct 
 * 
 * @param mem_base : the assigned memory for lcd struct
 * @param display : the array containing all the pixels the LCD screen has
 * @param large_mode : bool containing information whether display contents are to be viewed in an eyesight-malfunction-friendly mode
 * @param rendering_frame : bool standing for whether frame is rendering or what not
 * 
 * meant to be static
 */ 
typedef struct {
    void* mem_base;
    lcd_pixel display[WIDTH*HEIGHT];
    bool large_mode;
    bool rendering_frame;
} lcd_struct;

/**
 * function write_pixel_lcd
 * 
 * takes coordinates of a particular pixel and sets it to @param color
 */ 
void write_pixel_lcd(int x, int y,uint16_t color);

/**
 * function init_lcd
 * 
 * initializes the lcd struct once, sets fixed memory address etc.
 * also creates pthread_t render_thread_id for rendering to run on
 * 
 */
void init_lcd();

/**
 * function display_lcd_thread
 * 
 * thread-function for rendering, rewrites LCD display data depending on rendering_frame bool veracity
 */ 
void* display_lcd_thread(void *vargp);

/**
 * function display_lcd
 * 
 * sets rendering_frame to true
 */
void display_lcd();

/**
 * function clear_lcd
 * 
 * sets the whole display area to blue cyan color
 */
void clear_lcd();

/**
 * function add_text_lcd
 * 
 * adds @param text written in @param color, starts at [@param baseX, @param baseY]
 * font style depends on @param bold, function distinguishes large_mode
 * 
 */ 
void add_text_lcd(int baseX, int baseY, char* text, bool bold,uint16_t color);

/**
 * function get_color
 * 
 * @authors : Zdenek David, Lucka Veverkova
 * @return uint16_t lcd_pixel containing values of @param red, @param green and @param blue 
 */ 
lcd_pixel get_color(uint8_t red,uint8_t green,uint8_t blue);

/**
 * function get_hsv_color
 * 
 * @returns lcd_pixel (16 bites) coded in rgb spectrum with RGB values converted from
 * @param hue, @param saturation, @param value
 * 
 * uses hsv_to_rgb function, meant for display
 */ 
lcd_pixel get_hsv_color(uint8_t hue,uint8_t saturation,uint8_t value);


/**
 * function get_hsv_light_color
 * 
 * @returns lcd_pixel (32 bites) coded in rgb spectrum with RGB values converted from
 * @param hue, @param saturation, @param value
 * 
 * uses hsv_to_rgb function, meant for diodes
 */
uint32_t get_hsv_light_color(uint8_t hue,uint8_t saturation,uint8_t value);

/**
 * function hsv_to_rgb
 * 
 * converts params @param H, @param S, @param V to rgb spectrum, stores values in @param red, @param green, @param blue
 */ 
void hsv_to_rgb(uint8_t H, uint8_t S, uint8_t V, uint8_t* red,uint8_t* green,uint8_t* blue);


/**
 * function switch_large_mode
 * 
 * switches bool large_mode to another value
 */ 
void switch_large_mode();

/**
 * function dim_lcd
 * 
 * dims frame buffer to darker color
 * converts 16bit pixel values from display to separate 8bit R, G, B
 * changes values, converts them back via get_color function
 *   
 */ 
void dim_lcd(short dim);

/**
 * function dim_rect_lcd
 * 
 * only for aesthethic purposes, takes rectangle (width x height) starting at [@param x, @param y]
 * draws it in @param dim color
 * 
 */ 
void dim_rect_lcd(int x, int y, int width,int height,short dim);

/**
 * function is_rendering
 * 
 * @return lcd.rendering_frame
 */
bool is_rendering();