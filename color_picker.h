#ifndef COLOR_PICKER
#define COLOR_PICKER
#include <stdint.h>
#include <stdlib.h>

#include "potenciometer.h"

/**
 * color_picker_struct
 * 
 * contains variables in which the hue, saturation and value
 * for the picker are stored 
 * 
 * @param hue
 * @param saturation
 * @param value
 */ 
typedef struct {
    uint8_t hue;
    uint8_t saturation;
    uint8_t value;
} color_picker_struct;

/**
 * function init_color_picker
 * 
 * sets memory space for the color_picker_struct
 * default values for the params (hue, value, saturation = 0)
 * 
 * @return the defined struct
 */ 
color_picker_struct* init_color_picker();

/**
 * function set_color_picker_value
 * 
 * sets values in the @param color_picker to the params (@hue, @value, @saturation) it takes 
 */
void set_color_picker_value(color_picker_struct* color_picker,uint8_t hue,uint8_t saturation,uint8_t value);

/**
 * function update_color_picker_value
 * 
 * @param color_picker : pointer to the structure that is going to be updated
 * @param knobs : pointer to the knob structure, gets info about an amount knobs have turned 
 */ 
void update_color_picker_value(color_picker_struct* color_picker,knobs_struct* knobs);
#endif
