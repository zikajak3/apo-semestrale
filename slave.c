#include "slave.h"

static slave_struct slave;


void init_slave(bool* is_master,bool * is_slave,bool * is_connected,light_struct* left,light_struct* right) {
    

    slave.is_master = is_master;
    slave.is_slave = is_slave;
    slave.is_connected = is_connected;
    slave.thread_id = 0;
    slave.out_config_size = 0;
    slave.left_light = left;
    slave.right_light = right;
    

    // slave.in_config = calloc(1,sizeof(struct sockaddr_in));
    // slave.out_config = calloc(1,sizeof(struct sockaddr_in));

    memset(slave.buffer,0,sizeof(slave.buffer));
}

void receive_message_slave() {
    // usleep(1000000);
    // printf("awaiting message\n");
    memset(slave.buffer,0,sizeof(slave.buffer));
    //int size;
    // struct sockaddr_in temp;
    // memset(&temp,0,sizeof(temp));
    slave.out_config_size = sizeof(slave.out_config);
    // memset(&slave.out_config, 0, sizeof(slave.out_config));
    
    ssize_t received = recvfrom(slave.sockfd,slave.buffer,sizeof(slave.buffer),0,(struct sockaddr*)&slave.out_config,&slave.out_config_size);
    if(received == -1) {
        printf("receive_message_slave - %s\n",strerror(errno));
    }
    // printf("out config size:%d\n",slave.out_config_size);
    printf("from: %s\n",inet_ntoa(slave.out_config.sin_addr));
}

void send_message_slave(message_type type) {
    printf("slave-sending message %d\n",type);
    uint8_t send_type = (uint8_t)type;
    printf("send type %d\n",send_type);

    memset(slave.buffer,0,sizeof(slave.buffer));
    memcpy(slave.buffer, &send_type,sizeof(uint8_t));

    printf("to %s\n",inet_ntoa(slave.out_config.sin_addr));

    ssize_t sent = sendto(slave.sockfd,slave.buffer,16,0,(const struct sockaddr*)&slave.out_config,slave.out_config_size);
    if(sent == -1) {
        printf("send_message_slave %s\n",strerror(errno));
    }
}

void parse_message_slave() {
    uint8_t type;
    memcpy(&type,slave.buffer,sizeof(uint8_t));
    // printf("got %d\n",type);
    switch(type) {
        case 1://DISCOVERY_C:
            printf("DISCOVERY_C\n");
            if(!*(slave.is_master)) {
                send_message_slave(RESPONSE_C);
            } else {
                printf("got discover message, ignoring because it was from me\n");
            }
            break;
        case 2://RESPONSE_C:
            printf("RESPONSE_C\n");        
            break;
        case 3://ENSLAVE_C:
            printf("ENSLAVE_C\n");
            if(!*(slave.is_master)) {
                *(slave.is_slave) = true;
                *(slave.is_connected) = true;
            }
            break;
        case 4://RELEASE_C:
            printf("RELEASE_C\n");        
            *(slave.is_slave) = false;
            break;
        case 5://SET_LIGHT_C:
            if(*(slave.is_slave)) {
                memcpy(slave.left_light,slave.buffer+1,sizeof(light_struct));
                memcpy(slave.right_light,slave.buffer+1+sizeof(light_struct),sizeof(light_struct));
                printf("SET_LIGHT_C\n");        
            } else {
                printf("wont set light, im free man\n");
                send_message_slave(RELEASE_C);                
            }
            break;
    }

}

void start_slave(){
    pthread_create(&slave.thread_id,NULL,slave_thread,NULL);
}

void *slave_thread(){
    init_socket_slave();
    while(1) {
        receive_message_slave();
        parse_message_slave();
        if(slave.is_slave) {
            usleep(100000);
        } else if(*slave.is_connected) {
            printf("releasing myself from your oath\n");
            send_message_slave(RELEASE_C);
            *slave.is_connected = false;
        }
        // if(slave.is_master)
    }
    send_message_slave(RELEASE_C);    
    return NULL;
}

void init_socket_slave(){
    if((slave.sockfd = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP)) == -1) {
        printf("error %s\n",strerror(errno));
        pthread_exit((int *)slave.thread_id);
    }

    memset(&slave.in_config, 0, sizeof(struct sockaddr_in));
    memset(&slave.out_config, 0, sizeof(slave.out_config));
    
    slave.in_config.sin_family = AF_INET;
    slave.in_config.sin_port = htons(SLAVE_PORT);
    slave.in_config.sin_addr.s_addr = htonl(INADDR_ANY);

    slave.out_config.sin_family = AF_INET;

    if (bind(slave.sockfd, (struct sockaddr *)&slave.in_config, sizeof(slave.in_config)) == -1) {
        printf("bind %s\n",strerror(errno));
        pthread_exit((void *)slave.thread_id); //?
    }

    //int yes=1;

    // if (setsockopt(slave.sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
    //             sizeof(yes)) == -1) {
    //     printf("setsockopt (SO_REUSEADDR)\n");
    //     pthread_exit(slave.thread_id);
    // }

    
}