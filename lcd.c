#include "lcd.h"

static lcd_struct lcd;

void write_pixel_lcd(int x, int y,uint16_t color) {
  lcd.display[x+y*WIDTH] = color;
}

void init_lcd() {
    lcd.mem_base = map_phys_address(PARLCD_REG_BASE_PHYS,PARLCD_REG_SIZE,0);
    lcd.large_mode = false;
    parlcd_hx8357_init(lcd.mem_base);
    parlcd_write_cmd(lcd.mem_base,0x2c);
    clear_lcd();
    lcd.rendering_frame = false;
    pthread_t render_thread_id;
    pthread_create(&render_thread_id,NULL,display_lcd_thread,NULL);
}


void* display_lcd_thread(void *vargp) {
  while(1) {
    usleep(100);
    if(lcd.rendering_frame) {
      for(int i = 0; i < WIDTH*HEIGHT;i++) {
        parlcd_write_data(lcd.mem_base,lcd.display[i]);
      }
    }
    lcd.rendering_frame = false;
  }
}

void display_lcd() {
    lcd.rendering_frame = true;
}

void clear_lcd() {
  uint16_t color = get_color(0x62,0x92,0x9E);
  for(int i = 0; i < WIDTH*HEIGHT;i++) {
    lcd.display[i] = color;
  }
}

bool is_rendering() {
  return lcd.rendering_frame;
}

void dim_lcd(short dim) {
  uint16_t new_color;
  for(int i = 0; i < WIDTH*HEIGHT; i++) {
    new_color = lcd.display[i];

    uint8_t red = (new_color & 0b1111100000000000)>>8;
    uint8_t green = ((new_color << 5) & 0b1111110000000000)>>8;
    uint8_t blue = ((new_color << 11) & 0b1111100000000000)>>8;

    red = red - dim < 0 ? 0 : red-dim;
    green = green - dim < 0 ? 0 : green-dim;
    blue = blue - dim < 0 ? 0 : blue-dim;

    lcd.display[i] = get_color(red,green,blue);
  }
}


void dim_rect_lcd(int x, int y, int width,int height,short dim) {
    uint16_t new_color;   
    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
          
          new_color = lcd.display[(i+y)*WIDTH + j+x];

          uint8_t red = (new_color & 0b1111100000000000)>>8;
          uint8_t green = ((new_color << 5) & 0b1111110000000000)>>8;
          uint8_t blue = ((new_color << 11) & 0b1111100000000000)>>8;

          red = red - dim < 0 ? 0 : red-dim;
          green = green - dim < 0 ? 0 : green-dim;
          blue = blue - dim < 0 ? 0 : blue-dim;

          lcd.display[(i+y)*WIDTH + j+x] = get_color(red,green,blue);
        }
    }
}
 
void add_text_lcd(int baseX, int baseY, char* text, bool bold, uint16_t color) {
  int len = strlen(text);
  font_descriptor_t font;

  int x = baseX, y=baseY;

  if(bold) {
    font = font_winFreeSystem14x16;
  } else {
    font = font_rom8x16;
  }

  for(int i = 0; i < len; i++) {
    char c = text[i];
    c -= font.firstchar;

    // for each row in char
    for(int k = 0; k < font.height;k++) {
      font_bits_t row = font.bits[c*font.height + k];
      bool is;
      // for each pixel in row
      for(int l = 0; l < font.height ;l++) {
        is = (row << l) & 0b1000000000000000;
        if(is) {
          if(!lcd.large_mode) {
            write_pixel_lcd(x+l,y+k,color);
          } else {
            write_pixel_lcd(x+l*2,y+k*2,color);
            write_pixel_lcd(x+l*2,y+1+k*2,color);
            write_pixel_lcd(x+1+l*2,y+k*2,color);
            write_pixel_lcd(x+1+l*2,y+1+k*2,color);
          }
        }
      }
    }

    if(font.width == NULL) {
      if(lcd.large_mode) {
        x += font.maxwidth;
      }
      x += font.maxwidth;      
    } else {
      if(lcd.large_mode) {
        x += font.width[(int)c];
      }
      x += font.width[(int)c];      
    }
    if(lcd.large_mode) {
      x += 2;      
    }
    x += 2;
  }
}

lcd_pixel get_color(uint8_t red,uint8_t green,uint8_t blue) {
  uint16_t rgb;
  rgb = ((red & 0b11111000)<<8) | ((green & 0b11111100) << 3) | (blue >> 3);
  return rgb;
}

uint16_t color_for_frame(uint32_t color){
	return (((color>>3)&0x1F) | (((color>>10)&0x3F)<<5)) | (((color>>19)&0x1F)<<11); // 24bit color to 16bit color
}

uint16_t get_hsv_color(uint8_t H, uint8_t S, uint8_t V) {
  uint8_t r,g,b;
  hsv_to_rgb(H,S,V,&r,&g,&b);
	
	return get_color(r,g,b);
}

uint32_t get_hsv_light_color(uint8_t H, uint8_t S, uint8_t V) {
  uint8_t r,g,b;
  hsv_to_rgb(H,S,V,&r,&g,&b);

  uint32_t color = 0;

  color += r;
  color = color << 8;
  
  color += g;
  color = color << 8;

  color += b;
  return color;
}

void hsv_to_rgb(uint8_t H, uint8_t S, uint8_t V, uint8_t* red,uint8_t* green,uint8_t* blue) {
	float r = 0, g = 0, b = 0;
	
	float h = (float)H / 255.0f;
	float s = (float)S / 255.0f;
	float v = (float)V / 255.0f;
	
	int i = floor(h * 6);
	float f = h * 6 - i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);
	
	switch (i % 6) {
		case 0: r = v, g = t, b = p; break;
		case 1: r = q, g = v, b = p; break;
		case 2: r = p, g = v, b = t; break;
		case 3: r = p, g = q, b = v; break;
		case 4: r = t, g = p, b = v; break;
		case 5: r = v, g = p, b = q; break;
	}

  *red = (uint8_t)(r*255);
  *green = (uint8_t)(g*255);
  *blue = (uint8_t)(b*255);
}



void switch_large_mode() {
  lcd.large_mode = !lcd.large_mode;
}