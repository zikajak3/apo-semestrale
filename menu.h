#ifndef MENU
#define MENU
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


/**
 * enum options
 * 
 * used for distinguishing between the options dependent
 * on choice user made
 */ 

typedef enum {
    SET_STATIC,
    SET_DYNAMIC,
    SET_BLINKING,
    SET_LEFT,
    SET_RIGHT,
    SET_BOTH,
    SET_LEFT_DYNAMIC,
    SET_RIGHT_DYNAMIC,
    SET_BOTH_DYNAMIC,
    SET_BOTH_DYNAMIC_ANTIPHASE,    
    SET_LEFT_BLINKING,
    SET_RIGHT_BLINKING,
    SET_BOTH_BLINKING,
    DISABLE_BLINKING_LEFT,
    DISABLE_BLINKING_RIGHT,
    COPY_LEFT,
    COPY_RIGHT,
    BACK_MAIN_MENU,
    BE_MASTER,
    ENSLAVE,
    RELEASE,
    BACK,
    RESTORE_LEFT,
    RESTORE_RIGHT,
    RESTORE_BOTH,
} options;

/**
 * menu_item_struct
 * 
 * one item of the linked list
 * @param text : text item is descripted by on the LCD display
 * @param active : describes whether user is viewing this option/should be highlighted
 * @param hidden
 * @param option : option it contains
 */ 
typedef struct menu_item_struct {
    char* text;
    bool active;
    bool hidden;
    options option;
    struct menu_item_struct* previous;
    struct menu_item_struct* next;
} menu_item_struct;

/**
 * menu_struct
 * 
 * base of the cycled linked list
 * @param count : number of options 
 */ 
typedef struct menu_struct{
    int count;
    menu_item_struct* first;
    menu_item_struct* last;
    menu_item_struct* active;
} menu_struct;

/**
 * function create_menu
 * 
 * adds menu items to the linked list
 * uses init_menu, add_menu_item functions
 * 
 * @return the filled menu
 */  
menu_struct* create_menu();

/**
 * function init_menu
 * 
 * @return the initialized menu linked list
 */ 
menu_struct* init_menu();

/**
 * function add_menu_item
 * 
 * adds item described with @param text as @param option
 * returns NULL if option is already there
 */ 
void add_menu_item(menu_struct* menu, char* text, options option);

/**
 * function handle_options_turned
 * 
 * highlights the currently viewed option in the menu
 */ 
void handle_options_turned(menu_struct* menu,int8_t turned);
#endif