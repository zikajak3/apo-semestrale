#ifndef CONSTANTS_COMMUNICATION
#define CONSTANTS_COMMUNICATION
#define MASTER_PORT 82198
#define SLAVE_PORT 82209

/**
 * enum message_type
 * contains different values to be send in a packet
 * 
 * DISCOVERY_C : master tries to discover all potential slaves in a network
 * RESPONSE_C : packet includes a response to the discovery packet
 * ENSLAVE_C : packet tells another device to be enslaved 
 * RELEASE_C : packet contains confirmation of slave not being a slave anymore
 * SET_LIGHT_C : packet contains info about how slave's light settings are going to be changed
 */  
typedef enum {DISCOVERY_C=1,RESPONSE_C=2,ENSLAVE_C=3,RELEASE_C=4,SET_LIGHT_C=5} message_type;


#endif