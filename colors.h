
#define WHITE 0xFFFF
#define BLACK 0x0000

#define RED 0xf1c7
#define GREEN 0x4f8b
#define BLUE 0x3a5e

#define YELLOW 0xfee0

#define DARK_BG 0x4b4e
#define DARKER_BG 0x326a

#define WINDOW_COLOR 0x6cf5
