#ifndef BLINKING_GUI
#define BLINKING_GUI
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "potenciometer.h"

/**
 * blinking_gui_struct
 * 
 * contains variables describing different aspects
 * of diode blinking.
 * 
 * @param on : how long it takes for diode to be lit
 * @param off : how long it takes for diode to be not lit
 * @param shift : time difference in blinking between diode and time
 */
typedef struct {
    uint16_t on;
    uint16_t off;
    uint16_t shift;
} blinking_gui_struct;

/**
 * function init_blinking
 * 
 * sets memory space for the blinking_gui struct
 * default values for params (on, off, shift = 100)
 * 
 * @return the defined struct
 */
blinking_gui_struct* init_blinking();

/**
 * function update_blinking_value
 * 
 * @param blinking_gui : pointer to the structure that is going to be updated
 * @param knobs_struct : pointer to the knob structure, gets info about an amount knobs have turned 
 */ 
void update_blinking_value(blinking_gui_struct* blinking_gui,knobs_struct * knobs);
#endif