#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <stdbool.h>

#include "lights.h"
#include "constants.h"

typedef struct {
    int sockfd;
    pthread_t thread_id;
    struct sockaddr_in in_config;
    struct sockaddr_in out_config;
    unsigned int out_config_size;
    uint8_t buffer[256];
    bool * is_master;
    bool * is_slave;
    light_struct* left_light;
    light_struct* right_light;
    bool * is_connected;
} slave_struct;

void init_slave(bool* is_master,bool * is_slave,bool * is_connected,light_struct* left,light_struct* right);

void start_slave();

void *slave_thread();

void init_socket_slave();