#ifndef LIGHTS
#define LIGHTS
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h> 

#include "dynamic_lighting_gui.h"
#include "blinking_gui.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"

/**
 * enum lightning_type
 * 
 * flags for the state of LED diod
 */ 
typedef enum {STATIC,DYNAMIC,DYNAMIC_ANTIPHASE} lighting_type;


/**
 * light_struct
 * 
 * contains params desribing the diode light outcome
 * 
 */ 
typedef struct __attribute__((packed)) {
    uint8_t hue;
    uint8_t saturation;
    uint8_t value;
    uint8_t from_hue;
    uint8_t from_value;
    uint8_t from_saturation;
    uint8_t to_hue;
    uint8_t to_value;
    uint8_t to_saturation;
    uint16_t speed;
    bool is_antiphase;

    bool is_blinking;
    uint16_t on;
    uint16_t off;
    uint16_t shift;

    int id; //0 for left, 1 for right
    lighting_type state;
} light_struct;

/**
 * lights_pair_struct
 * 
 * contains light structs for both LED diodes
 */ 
typedef struct {
    light_struct* left;
    light_struct* right;
} lights_pair_struct;

/**
 * enslave_struct
 * 
 * params meant to be sent to the thread during enslaving process initialization
 * @param *id : IP address of slave 
 */ 
typedef struct {
    light_struct* left;
    light_struct* right;
    uint8_t *id;
} enslave_struct;

/**
 * function init_light
 * 
 * sets memory for light_struct of left/right diode (@param index)
 * sets all @params to 0 (except for @param on, @param off = 100)
 * 
 * @return the defined struct
 */ 
light_struct* init_light(int index);

/**
 * function init_mem_base
 * 
 * sets mem_base, uses map_phys_address
 */ 
void init_mem_base();

/**
 * function get_light_color
 * 
 * returns value calculated from @params r, g, b
 */ 
uint32_t get_light_color(uint8_t r,uint8_t g,uint8_t b);

/**
 * function hsv_to_rgb_lights
 * 
 * converts params @param H, @param S, @param V to rgb spectrum, stores values in @param red, @param green, @param blue
 */ 
void hsv_to_rgb_lights(uint8_t H, uint8_t S, uint8_t V, uint8_t* red,uint8_t* green,uint8_t* blue);

/**
 * functions static_lights_set, dynamic lights_set, blinking_set
 * 
 * sets light_structure @param light to other @params it takes
 */
void static_lights_set(light_struct *light,uint8_t hue,uint8_t saturation,uint8_t value);

void dynamic_lights_set(light_struct *light,dynamic_lighting_gui_struct* dynamic_gui);

void blinking_set(light_struct *light,blinking_gui_struct* blinking_gui);

/**
 * functions show_static_light, show_dynamic_light, show_light
 * 
 * show_static_light and show_dynamic_light use show_light function
 * to actually change the light value
 */ 
void show_static_light(light_struct* lights,clock_t time);

void show_dynamic_light(light_struct* lights, clock_t time);

void show_light(light_struct* light,uint8_t r,uint8_t g,uint8_t b);

/**
 * function handle_light_state
 * 
 * runs show_*state*_light function depending on state of @param lights
 */ 
void handle_light_state(light_struct* lights,clock_t time);

/**
 * function handle_blinking
 * 
 * returns true if the light is supposed to be blinking (or just lit), false otherwise
 * blinking depends on @param time
 */ 
bool handle_blinking(light_struct* lights, clock_t time);

/**
 * function start_light_thread
 * 
 * sets memory of lights_pair_struct
 * creates thread for light (obviously)
 */ 
void start_light_thread(light_struct* left,light_struct* right);

/**
 * function handle_light_thread
 * 
 * called by start_light_thread
 */ 
void* handle_light_thread(void *vargp);

#endif