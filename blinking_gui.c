#include "blinking_gui.h"

blinking_gui_struct* init_blinking() {
    blinking_gui_struct* blinking_gui = calloc(1,sizeof(blinking_gui_struct));

    blinking_gui->on = 100;
    blinking_gui->off = 100;
    blinking_gui->shift = 100;

    return blinking_gui;
}

void update_blinking_value(blinking_gui_struct* blinking_gui,knobs_struct* knobs) {

    int16_t on = blinking_gui->on + knobs->red_turned*10;
    int16_t off = blinking_gui->off + knobs->green_turned*10;
    int16_t shift = blinking_gui->shift + knobs->blue_turned*10;

    if(on < 1) {
        on = 1;
    }

    if(off < 1) {
        off = 1;
    }

    if(shift < 0) {
        shift = 0;
    }

    blinking_gui->on = on;
    blinking_gui->off = off;
    blinking_gui->shift = shift;

}
