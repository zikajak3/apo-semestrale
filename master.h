#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <stdbool.h>
#include <time.h>
#include "lights.h"
#include "constants.h"
#include "menu.h"

typedef struct {
    int sockfd;
    pthread_t thread_id;
    pthread_t enslave_thread;
    struct sockaddr_in in_config;
    struct sockaddr_in out_config;
    struct sockaddr_in incoming_slave[40];
    uint8_t num_of_slaves;
    uint8_t buffer[256];
    bool * is_slave;
    bool * is_master;
    bool * is_connected;
    menu_struct* menu;
    bool started; 
    bool inicialized;
    bool send_discover;
} master_struct;


void init_master(bool* is_slave,bool* is_master,bool * is_connected,menu_struct* menu);

void start_master();

void *master_thread();
void *enslave_master_thread(void *vargs);
void init_socket_master();

void start_enslave(uint8_t* slave_id,light_struct* left,light_struct* right);

void init_socket_out_master();

void init_socket_in_master();

void parse_message_master();

ssize_t  receive_message_master();

void send_message_enslave_master(message_type type, int id, int size);

void send_message_master(message_type type);

// Returns hostname for the local computer 
void check_host_name(int hostname);

// Returns host information corresponding to host name 
void check_host_entry(struct hostent * hostentry);

void check_IP_buffer(char *IPbuffer);

struct sockaddr_in sock_init();

void test_IP_addr();


