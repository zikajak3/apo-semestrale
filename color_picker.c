#include "color_picker.h"

color_picker_struct* init_color_picker() {
    color_picker_struct* color_picker = calloc(1,sizeof(color_picker_struct));
    color_picker->hue = 0;
    color_picker->value = 0;
    color_picker->saturation = 0;
    return color_picker;
}

void set_color_picker_value(color_picker_struct* color_picker,uint8_t hue,uint8_t saturation,uint8_t value) {
    color_picker->hue = hue;
    color_picker->saturation = saturation;
    color_picker->value = value;
}

void update_color_picker_value(color_picker_struct* color_picker,knobs_struct* knobs) {
    short new_hue,new_saturation,new_value;
    
    new_hue = color_picker->hue + (2*(knobs->red_turned));
    new_saturation = color_picker->saturation + 10*knobs->green_turned;
    new_value = color_picker->value + 10*knobs->blue_turned;


    if(new_saturation > 255) {
        new_saturation = 255;
    }
    if(new_value > 255) {
        new_value = 255;
    }


    if(new_saturation < 0) {
        new_saturation = 0;
    }
    if(new_value < 0) {
        new_value = 0;
    }

    color_picker->hue = new_hue%255;
    color_picker->saturation = new_saturation;
    color_picker->value = new_value;
}