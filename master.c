#include "master.h"


static master_struct master;

void print_bytes(void *ptr, int size) 
{
    unsigned char *p = ptr;
    int i;
    for (i=0; i<size; i++) {
        printf("%02hhX ", p[i]);
    }
    printf("\n");
}

void init_master(bool* is_slave,bool* is_master,bool * is_connected,menu_struct* menu) {
    master.is_slave = is_slave;
    master.is_master = is_master;
    master.is_connected = is_connected;
    master.thread_id = (int)NULL;

    master.menu = menu;

    memset(master.buffer,0,sizeof(master.buffer));
    memset(master.incoming_slave,0,sizeof(master.incoming_slave));
    master.num_of_slaves = 0;
    master.started = false;
    master.send_discover = true;

    master.inicialized = false;
}


void start_master() {
    
    if(!master.inicialized) {
        init_socket_master();
        master.inicialized = true;
    }

    master.send_discover = true;
    if(!master.started) {
        pthread_create(&master.thread_id,NULL,master_thread,NULL);
    }
}

// void start_enslave(uint8_t* slave_id,light_struct* left,light_struct* right) {
//     // pthread_join(master.thread_id,NULL);
//     enslave_struct* enslave_data = calloc(1,sizeof(enslave_struct));
//     enslave_data->id = slave_id;
//     enslave_data->left = left;
//     enslave_data->right = right;
//     printf("joined\n");
//     pthread_create(&master.enslave_thread,NULL,enslave_master_thread,enslave_data);
// }

void start_enslave(uint8_t* id,light_struct* left,light_struct* right) {
    printf("joined\n");
    enslave_struct* enslave_data = calloc(1,sizeof(enslave_struct));
    enslave_data->id = id;
    enslave_data->left = left;
    enslave_data->right = right;

    pthread_create(&master.enslave_thread,NULL,enslave_master_thread,enslave_data);   
}

void *enslave_master_thread(void *vargs) {
    *master.is_connected = true;
    enslave_struct* enslave_data = vargs;
    // uint8_t* id_pointer = vargs;
    uint8_t id = *(enslave_data->id);
    printf("enslaving %d\n",id);

    send_message_enslave_master(ENSLAVE_C,id,0);
    

    while(*(master.is_master)) {
        if(!*(master.is_master)) {
            printf("master\n");
            // break;
        }
        usleep(400000);
        memcpy(master.buffer+1,enslave_data->left,sizeof(light_struct));
        memcpy(master.buffer+1+sizeof(light_struct),enslave_data->right,sizeof(light_struct));
        send_message_enslave_master(SET_LIGHT_C,id,sizeof(light_struct)*2+10);
    }
    printf("releasing\n");
    send_message_enslave_master(RELEASE_C,id,0);
    *master.is_connected = false;
    return NULL;
}

void *master_thread() {
    
    if(master.send_discover) {
        master.started = true;
        send_message_master(DISCOVERY_C);       
        master.send_discover = false;         
    }

    printf("sending msg\n");
    //clock_t counter = clock() / (CLOCKS_PER_SEC / 1000);    

    //clock_t start = counter;
    int i = 0;
    while(1) {
        // printf("awaiting\n");
        if(receive_message_master() != -1) {
            parse_message_master();
        }
        if(master.send_discover) {
            send_message_master(DISCOVERY_C);       
            master.send_discover = false;         
        }
        //counter = clock() / (CLOCKS_PER_SEC / 1000);
        i++;
    }
    

    return NULL; 
}

    //send broadcast

    // wait for responses
    // clock_t counter = clock() / (CLOCKS_PER_SEC / 1000);
    // while(counter < 5000) {
        

    //     clock_t counter = clock() / (CLOCKS_PER_SEC / 1000);
    // }

void send_message_master(message_type type) {
    uint8_t send_type = (uint8_t)type;
    
    memcpy(master.buffer,&send_type,sizeof(uint8_t));

    socklen_t addr_len = sizeof(master.out_config);
    if(sendto(master.sockfd,master.buffer,2,0, (const struct sockaddr*)&master.out_config,addr_len) == -1) {
        printf("send_message_master - %s\n",strerror(errno));
    }
    printf("sent\n");
    memset(master.buffer,0,sizeof(master.buffer));    
}

void send_message_enslave_master(message_type type, int id, int size) {
    if(size == 0) {
        size = 2;
    }
    uint8_t send_type = (uint8_t)type;
    memcpy(master.buffer,&send_type,sizeof(uint8_t));
    socklen_t addr_len = sizeof(master.out_config);
    printf("sending - %d\n",type);
    if(sendto(master.sockfd,master.buffer,size,0, (const struct sockaddr*)&(master.incoming_slave[id]),addr_len) == -1) {
        printf("send_message_master - %s\n",strerror(errno));
    }
    memset(master.buffer,0,sizeof(master.buffer));
}

ssize_t receive_message_master() {
    memset(master.buffer,0,sizeof(master.buffer));
    socklen_t size;
    ssize_t received = recvfrom(
        master.sockfd,
        master.buffer,
        sizeof(master.buffer),
        0,
        (struct sockaddr*)&(master.incoming_slave[master.num_of_slaves]),
        &size
    );
    if(received == -1) {
        // printf("receive_message_master - %s\n",strerror(errno));
    }
    return received;
    // printf("master - from: %s\n",inet_ntoa(master.incoming_slave[master.num_of_slaves].sin_addr));
    
}

void parse_message_master() {
    uint8_t type;
    memcpy(&type,master.buffer,sizeof(uint8_t));
    char* ip_addr;
    
    switch(type) {
        case 2:
            ip_addr = calloc(32,sizeof(char));
            sprintf(ip_addr,"slave:%s",inet_ntoa(master.incoming_slave[master.num_of_slaves].sin_addr));
            memcpy(ip_addr+25,&master.num_of_slaves,1);
            add_menu_item(master.menu,ip_addr,ENSLAVE);
            master.num_of_slaves++;
            printf("got response\n");
            break;
        case 4:
            printf("\n\nwants to be released\n\n");
            *(master.is_master) = false;
            *(master.is_connected) = false;
        default:
            printf("other %d\n",type);
    }
}

void init_socket_master() {
    if((master.sockfd = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP)) == -1) {
        printf("error %s\n",strerror(errno));
        pthread_exit((int *)master.thread_id);
    }

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    if (setsockopt(master.sockfd, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
        perror("Error");
    }

    init_socket_in_master();
    init_socket_out_master();
}

void init_socket_in_master() {
    
    memset(&master.in_config, 0,  sizeof(master.in_config));
    master.in_config.sin_family = AF_INET;
    master.in_config.sin_port = htons(MASTER_PORT);
    master.in_config.sin_addr.s_addr = INADDR_ANY;

    if (bind(master.sockfd, (struct sockaddr *)&master.in_config, sizeof(master.in_config)) == -1) {
        printf("bind %s\n",strerror(errno));
        pthread_exit((int *)master.thread_id);
    }

    int on=1;
    setsockopt(master.sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));

    //int yes=1;

    // if (setsockopt(master.sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
    //             sizeof(yes)) == -1) {
    //     printf("setsockopt (SO_REUSEADDR)\n");
    //     pthread_exit(master.thread_id);
    // }
}

void init_socket_out_master() {

    memset(&master.out_config, 0, sizeof(master.out_config));
    master.out_config.sin_family = AF_INET;
    master.out_config.sin_port = htons(SLAVE_PORT);
    master.out_config.sin_addr.s_addr = INADDR_BROADCAST;

    // if (bind(master.sockfd, (struct sockaddr *)&master.out_config, sizeof(master.out_config)) == -1) {
    //     printf("bind %s\n",strerror(errno));    
    //     pthread_exit(master.thread_id);
    // }

}
