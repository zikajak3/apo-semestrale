#ifndef LIGHTING_GUI
#define LIGHTING_GUI
#include <stdint.h>
#include <stdbool.h>

#include "potenciometer.h"
#include "color_picker.h"

/**
 * enum dynamic_lightning_states
 * 
 * flags: 
 * 
 * FROM : the first light value changing to..
 * TO : ..the second light value
 * SPEED : speed of the dynamic light
 */ 
typedef enum {FROM=0,TO,SPEED} dynamic_lighting_states;

/**
 * dynamic_lightning_gui_struct
 * 
 * @param from_cp : color_picker_struct for the first light value
 * @param to_cp : color_picker struct for the second light value
 * @param speed : speed of light
 * @param state : FROM/TO/SPEED
 */ 
typedef struct {
    color_picker_struct* from_cp;
    color_picker_struct* to_cp;
    uint16_t speed;
    dynamic_lighting_states state;
} dynamic_lighting_gui_struct;

/**
 * function init_dynamic_lightning_gui
 * 
 * sets memory for the dynamic_lightning_gui struct
 * default values for params of the struct
 * (from_cp, to_cp = init_color_picker(), speed = 150, FROM)
 * 
 * @return the defined struct
 */ 
dynamic_lighting_gui_struct* init_dynamic_lighting_gui();


/**
 * function change_dynamic_color_picker_setting
 * 
 * enables user to choose which value will he change next via red knob (from light, to light, speed)
 * @param dynamic_gui : struct in which the state will be changed
 * @param red_clicked : whether user pushed the red knob
 */ 
void change_dynamic_color_picker_setting(dynamic_lighting_gui_struct* dynamic_gui,bool red_clicked);

/**
 * function update_dynamic_color_picker_value
 * 
 * @param dynamic_gui : struct to be updated, update depends on state struct has
 * @param knobs : knobs_struct whose values will update the color pickers 
 */ 
void update_dynamic_color_picker_value(dynamic_lighting_gui_struct* dynamic_gui,knobs_struct* knobs);
#endif