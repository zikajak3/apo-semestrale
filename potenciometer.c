#include "potenciometer.h"

knobs_struct* init_knobs() {
    knobs_struct *knobs = calloc(1,sizeof(knobs_struct));
    knobs->red_turned = knobs->blue_turned = knobs->green_turned = 2;
    knobs->red_pressed = knobs->blue_pressed = knobs->green_pressed = 0;
    
    knobs->mem_base = map_phys_address(SPILED_REG_BASE_PHYS,SPILED_REG_SIZE,0);
      if (knobs->mem_base == NULL) exit(1);
    uint32_t rgb_knobs_value = *(volatile uint32_t*)(knobs->mem_base + SPILED_REG_KNOBS_8BIT_o);

    knobs->red_pressed = knobs->green_pressed = knobs->blue_pressed = false;
    knobs->red_clicked = knobs->green_clicked = knobs->blue_clicked = false;

    knobs->previous_red = rgb_knobs_value & 0xFF;
    knobs->previous_green = (rgb_knobs_value>>8) & 0xFF;
    knobs->previous_blue = (rgb_knobs_value>>16) & 0xFF;
    return knobs;
}

void update_knob_values(knobs_struct* knobs){
    uint8_t new_red,new_green,new_blue;
    uint32_t rgb_knobs_value = *(volatile uint32_t*)(knobs->mem_base + SPILED_REG_KNOBS_8BIT_o);
    new_blue = (rgb_knobs_value & 0xFF) +2;
    new_green = ((rgb_knobs_value>>8) & 0xFF) +2;
    new_red = ((rgb_knobs_value>>16) & 0xFF) +2;
    
    bool blue_pressed = (rgb_knobs_value>>24) & 0x1;
    bool green_pressed = (rgb_knobs_value>>25) & 0x1;
    bool red_pressed = (rgb_knobs_value>>26) & 0x1;

    // usleep(1000);

    // printf("...\n");

    if(knobs->red_pressed && !red_pressed && !knobs->red_clicked) {
        usleep(1000);
        knobs->red_clicked = true;
    } else {
        knobs->red_clicked = false;
        
    }
    knobs->red_pressed = red_pressed;

    if(knobs->green_pressed && !green_pressed && !knobs->green_clicked) {
        usleep(1000);        
        knobs->green_clicked = true;
    } else {
        knobs->green_clicked = false;
    }
    knobs->green_pressed = green_pressed;

    if(knobs->blue_pressed && !blue_pressed && !knobs->blue_clicked) {
        usleep(1000);        
        knobs->blue_clicked = true;
    } else {
        knobs->blue_clicked = false;
    }
    knobs->blue_pressed = blue_pressed;

    //how much turned
    knobs->red_turned = ((int8_t)((new_red-(new_red%4))-(knobs->previous_red-(knobs->previous_red%4))))/4;
    knobs->green_turned = ((int8_t)((new_green-(new_green%4))-(knobs->previous_green-(knobs->previous_green%4))))/4;
    knobs->blue_turned = ((int8_t)((new_blue-(new_blue%4))-(knobs->previous_blue-(knobs->previous_blue%4))))/4;

    knobs->previous_red = new_red;
    knobs->previous_green = new_green;
    knobs->previous_blue = new_blue;
}