#include <stdio.h>
#include <stdlib.h>

#include "menu.h"
#include "colors.h"
#include "color_picker.h"
#include "lights.h"
#include "dynamic_lighting_gui.h"
#include "blinking_gui.h"
// #include "lcd.h"

/**
 * function init_pressed
 * 
 * pointer to blue knob pressed
 */ 
void init_pressed(bool* pressed_pnt);

/**
 * various render_*shape*_(*where*) functions
 */ 

void render_rectangle(int x, int y, int width,int height, uint16_t color);

void render_square(int x, int y, int size, uint16_t color);

void render_triangle_top_left(int x, int y, int size, uint16_t color);

void render_triangle_bottom_right(int x, int y, int size, uint16_t color);

void render_empty_square(int x, int y, int size, uint16_t color);

/**
 * function render_menu
 * 
 * renders menu to the frame buffer, considers large_mode on/off, 
 * higlights active item in the menu
 */ 
void render_menu(menu_struct* menu,int baseX, int baseY,bool large_mode);

/**
 * function render_status_bar
 * 
 * LED info at the bottom of the screen
 */ 
void render_status_bar(light_struct* left_light, light_struct* right_light,int baseX, int baseY,bool is_master,bool large_mode);

/**
 * function render_color_picker, dynamic_lightning_gui, blinking_gui
 * 
 * renders color_picker for the static/dynamic/blinking lightning gui 
 * at the top right corner of the screen
 */ 
void render_color_picker(color_picker_struct* color_picker, int baseX,int baseY,bool large_mode);

void render_dynamic_ligting_gui(dynamic_lighting_gui_struct* dynamic_gui, int x, int y, bool large_mode);

void render_blinking_gui(blinking_gui_struct* blinking_gui, int x, int y, bool large_mode);

/**
 * function render_exit_button
 * 
 * renders exit button
 */ 
void render_exit_button(int x, int y);